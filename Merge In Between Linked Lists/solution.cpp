/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */

ListNode * GetNodeByIndex(ListNode * list, int index) {
    int i = 0;
    if (index == 0) {
        return list;
    }
    
    ListNode* node = list;
    
    while (node) {
        if (i == index) {
            return node;
        }
        
        node = node->next;
        i++;
    }
    
    return nullptr;
}
    
ListNode * GetLastNode(ListNode * list) {
    ListNode * last = list;
    
    while(last->next) {
        last = last->next;
    }
    
    return last;
}
    
ListNode * mergeInBetween(ListNode * list1, int a, int b,
                          ListNode * list2)
{
    ListNode * a_th = GetNodeByIndex(list1, a - 1);
    ListNode * b_th = GetNodeByIndex(list1, b + 1);
    ListNode * last = GetLastNode(list2);
    
    a_th->next = list2;
    last->next = b_th;
    
    return list1;
}