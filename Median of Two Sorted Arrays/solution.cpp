std::vector<int> sumAndSort(const std::vector<int> & v1,
							const std::vector<int> & v2)
{
	std::vector<int> res = v1;
	res.insert(res.end(), v2.begin(), v2.end());
	
	std::sort(res.begin(), res.end());
	
	return res;
}

double findMedianSortedArrays(const std::vector<int> & v1,
							  const std::vector<int> & v2)
{
	std::vector<int> common = sumAndSort(v1, v2);
	size_t mid = common.size() >> 1;
	if (common.size() & 1) {
		return common.at(mid);
	} else {
		return ((static_cast<double>(common.at(mid)) + common.at(mid - 1)) / 2);
	}
}