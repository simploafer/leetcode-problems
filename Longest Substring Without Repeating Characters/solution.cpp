int lengthOfLongestSubstring(std::string s) {
    std::string substr;
    int res = 0;
    for (auto elem : s) {
        auto it = std::find(substr.begin(), substr.end(), elem);
        if (it == substr.end()) {
            substr.push_back(elem);
        } else {
            if (res < substr.size()) {
                res = substr.size();
            }

            substr.erase(substr.begin(), it + 1);
            substr.push_back(elem);
        }
    }

    if (res < substr.size()) {
        res = substr.size();
    }

    return res;
}