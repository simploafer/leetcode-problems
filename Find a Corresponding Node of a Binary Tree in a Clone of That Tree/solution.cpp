/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
    
public:
    
    TreeNode * m_ref;
    
    TreeNode * getTargetCopy(TreeNode * original, TreeNode * cloned, TreeNode * target) {
        if (original) {
            if (cloned->val == target->val) {
                m_ref = cloned;
                return m_ref;
            } else {
                getTargetCopy(original->left, cloned->left, target);
                getTargetCopy(original->right, cloned->right, target);
            }
        }
        
        return m_ref;
    }
};