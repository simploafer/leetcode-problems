/*Definition for singly-linked list.
struct ListNode {
	int val;
	ListNode *next;
	ListNode() : val(0), next(nullptr) {}
	ListNode(int x) : val(x), next(nullptr) {}
	ListNode(int x, ListNode *next) : val(x), next(next) {}
};*/

ListNode * addTwoNumbers(ListNode * l1, ListNode * l2) {
	short current;
	short temp;
	bool rest = 0;
	ListNode * head = l1;
	while (rest || l2) {
		temp = l2 ? l2->val : 0;
		current = l1->val + temp + rest;
		if (current > 9) {
			rest = 1;
			current -= 10;
		} else {
			rest = 0;
		}
		
		l1->val = current;
		if (l2 && l2->next) {
			l2 = l2->next;
		} else {
			l2 = nullptr;
		}
		
		if (l1->next) {
			l1 = l1->next;
		} else if (l2 || rest) {
			ListNode * newNode = new ListNode();
			l1->next = newNode;
			l1 = newNode;
		}
	}

	return head;
}